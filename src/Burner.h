#ifndef PUDUMMY_BURNER_H
#define PUDUMMY_BURNER_H

/**
 * @brief Defines a CPU burners for the MPPU dummy steering.
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a>,
 * <a href="mailto:Per.Werner@cern.ch">Per Werner</a>
 */

namespace pudummy {
  /**
   * Defines an object type that can burn CPU time. This object needs to be
   * calibrated before each run starts. It's recommended to instantiate it
   * during your system configuration phase begore multple threads/processes
   * are running on the host.
   */
  class Burner {

  public: ///< interface
    /**
     * Constructor, this will calibrate the burner.
     */
    Burner(void);
    ~Burner();

    /**
     * Burns time, returns a dummy value that might be ignored.
     *
     * @param micro_seconds The time to burn in microseconds.
     */
    double burn(unsigned int micro_seconds) const;
        
   private:
    Burner(const Burner&);
    unsigned int m_loopsPerUsec; ///< The number of loops to make per microssecond for LVL2
  };
}
#endif //PUDUMMY_BURNER_H

