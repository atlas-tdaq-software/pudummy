#ifndef PUDUMMY_ISSUES_H
#define PUDUMMY_ISSUES_H

/**
 * @brief ERS issues for the dummy implementation of HLTMPPU.
 * @author <a href="mailto:Per.Werner@cern.ch>Per Werner PH-ADT CERN</a>
 * Updated: $Id$
 */
 
#include "ers/ers.h"
#include <string>
#include <sstream>
#include "macros.h"

/// Generic Issue 
ERS_DECLARE_ISSUE(
  pudummy,
  Issue,
  ERS_EMPTY,
  ERS_EMPTY
)

ERS_DECLARE_ISSUE_BASE(
  pudummy,
  ConfigurationIssue,
  pudummy::Issue,
  " " << reason << oks_uid << ".",
  ERS_EMPTY,
  ((const char*) reason)
  ((const char*) oks_uid)
)

ERS_DECLARE_ISSUE_BASE(
  pudummy,
  ISInfoReceiverError,
  pudummy::Issue,
  " " << reason << " ISInfoReceiver subscription to IS object '"
  << is_object << "'. ",
  ERS_EMPTY,
  ((const char*) reason)
  ((const char*) is_object)
)

ERS_DECLARE_ISSUE_BASE(
  pudummy,          // namespace
  DuplicateROBID, // Exception name
  pudummy::Issue,   // base class name
  " ROB " << HEX(rob_id) << " is already definded.",
  ERS_EMPTY,
  ((uint32_t)rob_id) 
)

ERS_DECLARE_ISSUE_BASE(
  pudummy,
  AttributeNotFound,
  pudummy::Issue,
  " Ptree Configuration error: Attribute=" 
  << attribute << " not found for object type=" 
  << type << object_txt 
  << object_name << "." 
  << (ptree ? " Sub tree:\n" : "") << ptree << ".",
  ERS_EMPTY,
  ((const char*) attribute)
  ((const char*) type)
  ((const char*) object_txt)
  ((const char*) object_name)
  ((const char*) ptree)
)

ERS_DECLARE_ISSUE_BASE(
  pudummy,
  NoDataCollector,
  pudummy::Issue,
  " No data collector available.",
  ERS_EMPTY,
  ERS_EMPTY
)

ERS_DECLARE_ISSUE_BASE(
  pudummy,
  EformatFailure,
  pudummy::Issue,
  " Failed to create HLT result ROB for LVL1 ID="
  << lvl1_id << ".",
  ERS_EMPTY,
  ((uint32_t) lvl1_id)
)

ERS_DECLARE_ISSUE_BASE(
  pudummy,
  EformatCopyFailed,
  pudummy::Issue,
  " eformat::write::copy failed for LVL1 ID="
  << lvl1_id << ". Words written=" << words 
  << ", fragment size=" << fragment_size << ".",
  ERS_EMPTY,
  ((uint32_t) lvl1_id)
  ((unsigned int) words)
  ((unsigned int) fragment_size)
)

ERS_DECLARE_ISSUE_BASE(
  pudummy,
  EmptySubdetector,
  pudummy::Issue,
  " L1ID=" << lvl1_id << "(" << HEX(lvl1_id)
  << ") requested data from empty subdetector[Group] with ID " 
  << subdet << "(" << HEX(subdet) << ").",
  ERS_EMPTY,
  ((uint32_t) lvl1_id)
  ((unsigned int) subdet)
)

ERS_DECLARE_ISSUE_BASE(
  pudummy,    
  NotEnoughROBs, 
  pudummy::Issue,
  " pudummy requested " << asked_robs 
  << " ROBs but only "<< valid_robs 
  << " were available.",
  ERS_EMPTY,
  ((uint32_t)asked_robs) 
  ((size_t)valid_robs) 
)

ERS_DECLARE_ISSUE_BASE(
  pudummy,    
  FailedToRetrieveROBs, 
  pudummy::Issue,
  "Retrieved " << rob_cnt 
  << " of " << asked_robs
  << " requested ROBs in " << tries
  << " tries. There were " << valid_robs 
  << " ROBs available for selection"
  << " from subdetector[Group] " << HEX(sd) << ".",
  ERS_EMPTY,
  ((size_t)rob_cnt)
  ((uint32_t)asked_robs)
  ((size_t)tries)
  ((size_t)valid_robs)
  ((uint8_t)sd)
)

ERS_DECLARE_ISSUE_BASE(
  pudummy,
  EmptyCalibrationTag,
  pudummy::Issue,
  " For L1ID=" << lvl1_id 
  << ": step " << step
  << " calibration tag " << ctag << " had zero ROBs. Tag ignored.",
  ERS_EMPTY,
  ((unsigned int) lvl1_id)
  ((const char*) step)
  ((const char*) ctag)
)

ERS_DECLARE_ISSUE_BASE(
  pudummy,
  StreamTagError,
  pudummy::Issue,
  " Internal ERROR: Cannot find " << tag_t 
  << " " << tag << " for " 
  << stepType << " " << stepName << ".",
  ERS_EMPTY,
  ((const char*) tag_t)
  ((const char*) tag)
  ((const char*) stepType)
  ((const char*) stepName)
)

ERS_DECLARE_ISSUE_BASE(
  pudummy,
  MissingAlgorithm,
  pudummy::Issue,
  " Dummy Algorithm '"
  << step << "' was not found in internal maps.",
  ERS_EMPTY,
  ((const char*) step)
)

ERS_DECLARE_ISSUE_BASE(
  pudummy,
  ProcessException,
  pudummy::Issue,
  " " << type << ": " << mesg 
  << "' while processing event with lvl1_id=" 
  << lvl1_id << ".",
  ERS_EMPTY,
  ((const char*) type)
  ((const char*) mesg)
  ((unsigned int) lvl1_id)
)

ERS_DECLARE_ISSUE_BASE(
  pudummy,          // namespace
  DuplicateROBsSelected, // Exception name
  pudummy::Issue,   // base class name
  " " << removed_robs << " duplicate ROBs removed. "
  << "Probably multiple select to the same sub detector.",
  ERS_EMPTY,
  ((size_t)removed_robs) 
)
#endif //PUDUMMY_ISSUES_H
