/**
 * @brief The MPPU dummy steering implementation.
 * $Id$
 */

#include <sstream>

#include "TRandom.h"
#include "ipc/partition.h"

#include "hltinterface/DataCollector.h"
#include "hltinterface/EventId.h"
#include "eformat/write/eformat.h"
#include "eformat/write/FullEventFragment.h"
#include "eformat/write/node.h"
#include "eformat/eformat.h"
#include "CTPfragment/CTPfragment.h"
#include <is/type.h>
#include <is/infoT.h>

#include "Steering.h"
#include "Issues.h"
#include "macros.h"

namespace pudummy {

  struct HLTResult {

    static const size_t DEFAULT_MAX_RESULTSIZE = 1024 * 1024 * 20 / 4;

    HLTResult() : fragment_pointer(new uint32_t[DEFAULT_MAX_RESULTSIZE]) {}

    std::vector<uint32_t>                   trigger_info; // HLT trigger info words
    std::vector<eformat::helper::StreamTag> stream_tag;   // stream tags with list of ROBs/SDs for partial event building
    std::unique_ptr<uint32_t[]>             fragment_pointer; // Buffer where the HLT result(s) are stored.
   };

}

// static 
pudummy::Config * pudummy::Steering::s_config[PUDUMMY_NUM_CONFIGS] = {0,0};
size_t pudummy::Steering::s_activeConfig(0);
boost::mutex pudummy::Steering::s_config_mutex;

pudummy::ISHelper::ISHelper() :
  lvl2_steps(0),
  ef_steps(0),
  ROBsAcceptedEvent(0),
  ROBsRejectedEvent(0)
{}

pudummy::ISHelper::~ISHelper() {
}

/************************************************/
static bool decide(double probability)
{
  return gRandom->Rndm() < probability;
}

/************************************************/
pudummy::Steering::result_t 
pudummy::Steering::partial_fill_result(const pudummy::DummyStep * alg,
                                       uint64_t global_id,
                                       const std::vector<eformat::read::ROBFragment >& l1r,
                                       pudummy::HLTResult& hltr)
                                                                   
{
  const pudummy::DummyStreamTag& pt = alg->get_physics_tag();
  const pudummy::DummyStreamTag& dt = alg->get_debug_tag();
  const pudummy::DummyCalStreamTag* ct = alg->get_calibration_tag(); 
	result_t result = ACCEPT;
  if (decide(dt.get_probability())) {
    hltr.stream_tag.push_back(eformat::helper::StreamTag(
        dt.get_uid(), eformat::DEBUG_TAG, false));
    ERS_DEBUG(1, "Step " << alg->get_uid() << " L1ID=" << global_id 
        << " got DEBUG tag " << dt.get_uid()
        << " with probability=" << dt.get_probability());
    return DEBUG;
  }

  if (ct && decide(ct->get_probability())) { 
    const pudummy::DataAccess * calibData = ct->get_calibrationData();    
    std::vector<uint32_t> ids;
    config()->get_ROB_IDs(global_id, *calibData,
         ids, alg->get_uid());
    if (ids.size() == 0) {
      m_is_info->emptyCalibrationTags++;
      if(!config()->ignore_bad_rob_access()) {
        ers::warning(pudummy::EmptyCalibrationTag(ERS_HERE, global_id,
              alg->get_uid().c_str(), ct->get_uid().c_str()));
      }
    } else {
      /* Add the calibration StreamTag with ROB IDs to the 
       * HLTResult. Requests for a SubDetector or  
       * SubDetectorGroup is resolved to the actual ROBs.  
       * This way the DCM only handleslists of ROBs.
       */   
      std::set<uint32_t> robs(ids.begin(), ids.end());
      std::set<eformat::SubDetector> sub_detectors;
      hltr.stream_tag.push_back(eformat::helper::StreamTag(ct->get_uid(),
           eformat::CALIBRATION_TAG, false, robs, sub_detectors)); 
      ERS_DEBUG(0, "For L1ID=" << global_id << " step " << alg->get_uid()  
        << " sets DummyCalStreamTag=" << ct->get_uid() << " with " << robs.size() << " ROBs.");
    }
  }
  if (decide(pt.get_probability())) {
    if (pt.get_uid() == "L1TTStreams" || pt.get_uid() == "L1TTStreamsNoRND") {
      // We use the L1TT to build the streams
      // First we have to find the CTP fragment
      std::vector<eformat::helper::StreamTag> ttags;
      ttags.push_back(eformat::helper::StreamTag("CTPNotFound",
						 eformat::DEBUG_TAG, false));

      bool random = false;
      for ( const auto & robdata: l1r ) {
        const eformat::read::ROBFragment rob(robdata);
        eformat::helper::SourceIdentifier sid(rob.rod_source_id());
        if ( sid.subdetector_id() == eformat::TDAQ_CTP ){
          uint32_t l1tt = rob.rod_lvl1_trigger_type();
          random = (l1tt == 0x81);
          ttags.clear();
          CTPfragment::streamTags(l1tt, ttags);
          break;
        }
      }
      
      hltr.stream_tag.clear();
      hltr.stream_tag.insert(hltr.stream_tag.end(), ttags.begin(), ttags.end());
      if (random && pt.get_uid() == "L1TTStreamsNoRND"){
        hltr.stream_tag.clear();
        result = REJECT;
      }

    } else {
      hltr.stream_tag.push_back(eformat::helper::StreamTag(pt.get_uid(),
							   eformat::PHYSICS_TAG, true));
      
      ERS_DEBUG(1, "Step " << alg->get_uid() << " L1ID=" << global_id 
        << " was accepted for PHYSICS tag " << pt.get_uid() 
        << " with probability=" << pt.get_probability() 
        << ". hltr.stream_tag.size()=" << hltr.stream_tag.size());
    }
  } else { 
    ERS_DEBUG(1, "Step " << alg->get_uid() << " L1ID=" << global_id 
        << " was rejected for PHYSICS tag " << pt.get_uid() 
        << " with probability=" << pt.get_probability());
    result = REJECT;
  }
  return result;
}

/************************************************/
static std::string detailed_decison_info(uint64_t global_id, 
                                         pudummy::HLTResult& hltr,
                                         uint32_t rob_size)
{
  std::ostringstream oss;
  for(std::vector<eformat::helper::StreamTag>::const_iterator 
      it = hltr.stream_tag.begin(); it != hltr.stream_tag.end(); ++it)
      oss << it->name << "@" << it->type << ", "; 

  std::ostringstream oss1;
  oss1 << "Detailed steering outcome for L1ID=" << global_id << ": "
    << "\t\n HLT TriggerInfo words = " << hltr.trigger_info.size()
    << "\t\n StreamTags entries    = " << hltr.stream_tag.size()
    << "\t\n StreamTags            = " << oss.str()
    << "\t\n StreamTags words      = " << eformat::helper::size_word(hltr.stream_tag)
    << "\t\n HLT result ROB words  = " << rob_size << std::endl;
  return oss1.str();
}

/************************************************/
pudummy::Steering::Steering()
  : m_ISConfigSub(""),
    m_runNumber(0),
    m_dc(0),
    m_tdaq_is_server("DF"),
    m_ISInfoReceiver(0),
    m_running(false)
{
  assert(PUDUMMY_NUM_CONFIGS >= 2);
}

/************************************************/
bool pudummy::Steering::configure(const boost::property_tree::ptree& args)
{
  ERS_DEBUG(0, "configure() called with ptree named='" << args.get_value<std::string>() << "'.");
  // Assure that all configurations are deleted. 
  if (s_config[(s_activeConfig+1)%PUDUMMY_NUM_CONFIGS]) { 
    ers::error(ConfigurationIssue(ERS_HERE, "INTERNAL ERROR: An unused configuration found and deleted at CONFIG", ""));
    delete s_config[(s_activeConfig+1)%PUDUMMY_NUM_CONFIGS];
    s_config[(s_activeConfig+1)%PUDUMMY_NUM_CONFIGS] = 0;
  }
  
  s_config[s_activeConfig] = new Config(args);
  if (m_ISConfigSub.size() == 0) {
    char * tmp = getenv("TDAQ_IS_SERVER");
    if (tmp) {
      m_tdaq_is_server = m_ISConfigSub = std::string(tmp);
    } 
    m_ISConfigSub.append(".HLTConf");
  }
  ERS_LOG("TDAQ_IS_SERVER=" << m_tdaq_is_server);
  if (config() == 0) {
    ers::fatal(pudummy::ConfigurationIssue(ERS_HERE, "Failed to create configuration", ""));
    throw(pudummy::ConfigurationIssue(ERS_HERE, "Failed to create configuration", ""));
  }
  return config()->status();
}

/************************************************/
bool pudummy::Steering::connect(const boost::property_tree::ptree& args)
{
  ERS_DEBUG(0, "connect() called with ptree named='" << args.get_value<std::string>() << "'.");
  return true;
}

/************************************************/
bool pudummy::Steering::prepareForRun(const boost::property_tree::ptree& args)
{
  ERS_DEBUG(0, "prepareForRun() called with ptree named='" << args.get_value<std::string>() << "'.");
  // Pick up the DataCollector here as HLTMPPU instantiates the DataCollector at end of configure. 
  m_dc = hltinterface::DataCollector::instance();
  if (!m_dc) return false;
  ERS_DEBUG(3, "DataCollector @ " << m_dc);
  return true;
}

/************************************************/
bool pudummy::Steering::stopRun(const boost::property_tree::ptree& args)
{
  ERS_DEBUG(0, "stopRun() called with ptree named='" << args.get_value<std::string>() << "'.");
  m_running = false;
  return true;
}

/************************************************/
bool pudummy::Steering::disconnect(const boost::property_tree::ptree& args)
{
  ERS_DEBUG(0, "disconnect() called with ptree named='" << args.get_value<std::string>() << "'.");
  return true;
}

/************************************************/
bool pudummy::Steering::unconfigure(const boost::property_tree::ptree& args)
{
  ERS_DEBUG(0, "unconfigure() called with ptree named=" << args.get_value<std::string>());
  for (size_t ix = 0; ix < PUDUMMY_NUM_CONFIGS; ix++) {
    delete s_config[ix];
  	s_config[ix] = 0;
  }
  s_activeConfig = 0;
  
  delete m_ISInfoReceiver; m_ISInfoReceiver = 0;
  
  ERS_DEBUG(4, "Dummy Steering unconfigure() finished.");
  return true;
}

/************************************************/
// Receive new configuration from IS.
#include <boost/property_tree/info_parser.hpp>

void pudummy::Steering::reconfig_IS_callback(ISCallbackInfo * icbi) 
{ 
  ISInfoString isi;
  /* 1) Get Config data.
   * 2) Lock reconfiguration mutex.
   * 2) Convert IS data to ptree.
   * 3) Create new pudummy::Config object.
   * 4) If successful {set unused s_config pointer}
   *    else {delete temp Config object and inject error message}
   */
  Config * configuration = 0;
  try {
    if (icbi->type() != isi.type()) {
      std::ostringstream oss;
      oss << "Wrong data type IS object '" << icbi->name() 
        << "' of type='" << icbi->type() 
        << "'. Expected type='String'" << std::endl;
      ers::warning(ConfigurationIssue(ERS_HERE, oss.str().c_str(), ""));
      return;
    }
    icbi->value(isi);
    ERS_LOG("New HLT configuration received via IS object='" << icbi->name()
      << "', type='" << icbi->type() << "' and value='" << isi.getValue() << "'.");
    
    if (!s_config_mutex.try_lock()) {
      ers::error(ConfigurationIssue(ERS_HERE, 
        "Pudummy reconfiguration is already locked. Ignore this reconfiguration attempt for IS object=",
        icbi->name()));
      return;
    }
    std::stringstream sstream;
    sstream << isi.getValue();
    boost::property_tree::ptree default_tree;
    boost::property_tree::ptree ptree;
    boost::property_tree::read_info(sstream, ptree, default_tree);
    if (ptree == default_tree) {
      ers::error(ConfigurationIssue(ERS_HERE, 
        "Conversion to ptree for reconfiguration failed for IS object=", 
        icbi->name()));
      s_config_mutex.unlock();
      return;
    }
    configuration = new Config(ptree);
  }
  catch (ers::Issue& ex) {
    ers::error(ConfigurationIssue(ERS_HERE, "Caught ers::Issue during reconfiguration from IS", "", ex));
    delete configuration; configuration = 0;
  }
  catch (std::exception& ex) {
    ers::error(ConfigurationIssue(ERS_HERE, "Caught std::exception during reconfiguration from IS", "", ex));
    delete configuration; configuration = 0;
  }
  // Activate the new configuration.
  s_config[(s_activeConfig+1)%PUDUMMY_NUM_CONFIGS] = configuration;
}

/************************************************/
/*
 * Function that is executed in postFork and runs
 * ISInfoReceivers in separate threads.
 */
void pudummy::Steering::run_is()
{
  const char * part_name = getenv("TDAQ_PARTITION");

  try { 
    IPCPartition p(part_name);
    m_ISInfoReceiver = new ISInfoReceiver(p);
    //m_ISInfoReceiver->subscribe(m_ISConfigSub, reconfig_IS_callback);
    ERS_LOG("Subscribed to " <<  m_ISConfigSub
        << " to trigger pudummy reconfiguration");
  }
  catch (ers::Issue& ex) {
    ers::error(pudummy::ISInfoReceiverError(ERS_HERE, 
      "ers::Issue when creating", m_ISConfigSub.c_str(), ex.what()));
  }
  catch (std::exception& ex) {
    ers::error(pudummy::ISInfoReceiverError(ERS_HERE, 
      "std::exception  when creating ", m_ISConfigSub.c_str(), ex.what()));
  }
}

bool pudummy::Steering::process(const std::vector<eformat::read::ROBFragment >& l1r,
                                uint64_t global_id,
                                pudummy::HLTResult& hltr)
{ 

  // Check if new configuration is available.
  if (s_config[(s_activeConfig+1)%PUDUMMY_NUM_CONFIGS]) {
    delete s_config[s_activeConfig];
    s_config[s_activeConfig] = 0;
    s_activeConfig = (s_activeConfig+1)%PUDUMMY_NUM_CONFIGS;
    std::ostringstream oss;
    oss << "New HLT configuration activated: Changed from configuration[" 
        << (s_activeConfig+1)%PUDUMMY_NUM_CONFIGS 
        << "] to configuration[" << s_activeConfig << "]";
    ers::info(pudummy::ConfigurationIssue(ERS_HERE, oss.str().c_str(), ""));
  }
  
  m_timeOut = false; 
  result_t result = FAILURE;
  std::vector<hltinterface::DCM_ROBInfo> data;
  uint32_t lvl1_id = 0;
  m_is_info->lvl1_events++;

  if (!config()->status()) {
    std::ostringstream oss;
    oss << "PuDummySteering has configuration status=" << bool2str(config()->status()) 
        << ". Event is not processed and rejected.";
    m_is_info->not_processed++;
    ers::error(pudummy::ProcessException(ERS_HERE, "No valid configuration", oss.str().c_str(), 0));
    return false;
  }

  if(l1r.size() == 0) {
    std::ostringstream oss;
    oss << "PuDummySteering has L1 result size = " << l1r.size()
        << ". Event is not processed and rejected.";
    m_is_info->not_processed++;
    ers::error(pudummy::ProcessException(ERS_HERE, "No LVL1 result", oss.str().c_str(), 0));
    return false;
  }

  // Invalidate previous HLT ROB (if any) and pick up the LVL1 ID. 
  hltr.fragment_pointer[0] = 0;
  lvl1_id = l1r[0].rod_lvl1_id();
  m_is_info->last_lvl1_id = lvl1_id;
  const std::vector<DummyStep*> & algs = config()->algorithms();
  ERS_DEBUG(1, "Start process() for event with global id=" << global_id);
  // Process event until reject or all algorithm steps executed.
  try {
    for(std::vector<DummyStep*>::const_iterator it = algs.begin(); 
        it != algs.end(); ++it) {
      result=run_alg(*it, global_id, data, l1r, hltr);
      if (m_timeOut || result!=ACCEPT) break;
    }
  }
  catch (ers::Issue & ex) {
    ers::error(ProcessException(ERS_HERE, "ERS::issue", ex.what(), global_id));
    return false;
  }
  catch (std::exception & ex) {
    ers::error(ProcessException(ERS_HERE, "std::exception", ex.what(), global_id));
    return false;
  }
  unsigned int hlt_rob_size = 0;
  if (m_timeOut) { 
    // Tag this event for DEBUG stream with processing timeout.
    ers::error(ProcessException(ERS_HERE, "Time out", "", global_id));
    m_is_info->processing_timeout++;
    m_is_info->accepted++;
    m_is_helper.ROBsAcceptedEvent += data.size();
    hltr.stream_tag.push_back(eformat::helper::StreamTag(
        "pudummy processing time out", eformat::DEBUG_TAG, false));
    hlt_rob_size = fill_hlt_result(l1r, hltr);
  } else if (result == ACCEPT) {
    hlt_rob_size = fill_hlt_result(l1r, hltr);
    m_is_info->accepted++;
    m_is_helper.ROBsAcceptedEvent += data.size();
  } else { // REJECT or DEBUG
    if (hltr.stream_tag.size()) {
      // Add IS counter for non physics accept ??
      hlt_rob_size = fill_hlt_result(l1r, hltr);
      m_is_info->accepted++; // not physics accept
      m_is_helper.ROBsAcceptedEvent += data.size();
    } else {
      m_is_info->rejected++;
      m_is_helper.ROBsRejectedEvent += data.size();
    }
  }
  std::ostringstream oss;
  
  ERS_DEBUG(1, "Final decision for event L1ID=" << global_id << " is " 
    << (hltr.stream_tag.size() ? "ACCEPT":"REJECT") 
    << ". Totally " << data.size() << " ROBs collected.\n");
  ERS_DEBUG(1, "" << detailed_decison_info(global_id, hltr, hlt_rob_size));
  // Update IS from class Config and m_is_helper
  m_is_info->notEnoughROBs = config()->notEnoughROBs();
  m_is_info->emptySubdetectorAccess = config()->emptySubdetectorAccess();
  uint64_t events = m_is_info->lvl1_events;
  uint64_t accepted = m_is_info->accepted;
  uint64_t rejected = m_is_info->rejected;
  double stepsPerEvent = events == 0 ? 0.0 : (double)(m_is_helper.lvl2_steps + m_is_helper.ef_steps)
      /events;
  double avgROBsAcceptedEvent = (double)(m_is_helper.ROBsAcceptedEvent)/accepted;
  double avgROBsRejectedEvent = (double)(m_is_helper.ROBsRejectedEvent)/rejected;
  m_is_info->stepsPerEvent = stepsPerEvent;
  m_is_info->avgROBsAcceptedEvent = avgROBsAcceptedEvent;
  m_is_info->avgROBsRejectedEvent = avgROBsRejectedEvent;
  return true;
}

bool pudummy::Steering::doEventLoop()
{
    m_running = true;
    bool exit_code = true;
    while(m_running) {

        try {
            std::unique_ptr<uint32_t[]> pl1r;
            
            switch(m_dc->getNext(pl1r)) {
            case hltinterface::DataCollector::Status::OK:
                break;
            case hltinterface::DataCollector::Status::NO_EVENT:
                sleep(1);
                continue;
            case hltinterface::DataCollector::Status::STOP:
                m_running = false;
                continue;
            }

            // retrieve L1 ROBs
            std::vector<eformat::read::ROBFragment> fragments;
            eformat::read::FullEventFragment l1r(pl1r.get());

            for(uint32_t i = 0; i < l1r.nchildren(); i++) {
                fragments.emplace_back(l1r.child(i));
            }

            // Process event and get HLT result
            HLTResult hltr;
            process(fragments, l1r.global_id(), hltr);

            // Prepare full event fragment for result
            eformat::write::FullEventFragment full_result;

            // Copy header completely from input event, HLT related parts will be overriden
            full_result.copy_header(l1r.start());

            // Convert HLTResult to FullEventFragment

            // 1. HLT trigger info
            full_result.hlt_info(hltr.trigger_info.size(), &hltr.trigger_info[0]);

            // 2. stream tags
            std::unique_ptr<uint32_t[]> tags;
            if(hltr.stream_tag.size()) {
                uint32_t tags_size = eformat::helper::size_word(hltr.stream_tag);
                tags.reset(new uint32_t[tags_size]);

                eformat::helper::encode(hltr.stream_tag, tags_size, tags.get());
                full_result.stream_tag(tags_size, tags.get());

                // 3. HLT Result ROBs - we have only one at hltr.fragment_pointer
                eformat::write::ROBFragment hltResult(hltr.fragment_pointer.get());
                full_result.append(&hltResult);
            }

            // Copy into continuous buffer
            auto nodes = full_result.bind();
            auto words = eformat::write::count_words(*nodes);

            std::unique_ptr<uint32_t[]> result(new uint32_t[words]);
            eformat::write::copy(*nodes, result.get(), words);
            
            // Pass to HLTMPPU
            m_dc->eventDone(std::move(result));

        } catch (ers::Issue& issue) {
            ERS_LOG("ERS exception caught in doEventLoop");
            ers::error(issue);
            exit_code = false;
            break; // If there is an error, we can't be sure memory is not corrupted, it's safest to exit
        } catch (...) {
            ERS_LOG("Unknown Exception caught in doEventLoop");
            // TODO: separate exceptions
            exit_code = false;
            break; // If there is an error, we can't be sure memory is not corrupted, it's safest to exit
        }
        
    }
    return exit_code;
}


void pudummy::Steering::logISInfo() 
{
  try {
    // Publish statistics 
    uint64_t events = m_is_info->lvl1_events;
    uint64_t accepted = m_is_info->accepted;
    uint64_t rejected = m_is_info->rejected;
    m_is_info->stepsPerEvent = events == 0 ? 0.0 : (double)(m_is_helper.lvl2_steps + m_is_helper.ef_steps)
        /events;
    m_is_info->avgROBsAcceptedEvent = (double)(m_is_helper.ROBsAcceptedEvent)/accepted;
    m_is_info->avgROBsRejectedEvent = (double)(m_is_helper.ROBsRejectedEvent)/rejected;
    std::ostringstream oss;
    m_is_info->print(oss);
    ERS_LOG("PuDummySteering IS info: " 
        << "\nlvl2_steps=" << m_is_helper.lvl2_steps
        << "\nef_steps=" << m_is_helper.ef_steps
        << "\nROBsAcceptedEvent=" << m_is_helper.ROBsAcceptedEvent
        << "\nROBsRejectedEvent=" << m_is_helper.ROBsRejectedEvent
        << "\n" << oss.str() << std::endl);
  }
  catch (ers::Issue& ex) { 
    throw(pudummy::ConfigurationIssue(ERS_HERE, "ers::Issue in logISInfo", "", ex));
  }
  catch (std::exception& ex) { 
    throw(pudummy::ConfigurationIssue(ERS_HERE, "std::exception in logISInfo", "", ex));
  }
}
/************************************************/
bool pudummy::Steering::hltUserCommand(const boost::property_tree::ptree& args)
{    
  ERS_LOG("Received hltUserCommand called with ptree named='" << args.get_value<std::string>() << "'.");
  return true;
}

/************************************************/
bool pudummy::Steering::publishStatistics(const boost::property_tree::ptree& args)
{
  ERS_LOG("Dummy Steering publishStatistics called with ptree named='" << args.get_value<std::string>() << "'.");
  logISInfo();
  return true;
}

/************************************************/
void pudummy::Steering::timeOutReached(const boost::property_tree::ptree& args)
{
  ERS_LOG("Dummy Steering timeOutReached calledcalled with ptree named='" 
    << args.get_value<std::string>() << "'.");
  // Set m_timeOut flag in Steering. Reset by Steering::process().
  m_timeOut = true;
}


/************************************************/
unsigned int pudummy::Steering::buildROBFragment(
      const std::vector<eformat::read::ROBFragment >& l1r,
      uint32_t* store) const
{
    size_t payLoad = config()->result_size();
    if (payLoad > HLTResult::DEFAULT_MAX_RESULTSIZE) {
      throw(pudummy::ConfigurationIssue(ERS_HERE, "HLT Result size in configuration larger than maximum", ""));
    }
    const uint32_t event_type = 1;
    uint32_t copy_size = 0;
    uint32_t rob_size = 0;
    uint32_t lvl1_id = l1r[0].rod_lvl1_id();
  try {
    eformat::helper::SourceIdentifier src(eformat::TDAQ_EVENT_FILTER, 0);
    // Build the dummy ROD data
    uint32_t * dummy = 0;
    if (payLoad) {
     dummy = new uint32_t[payLoad]; // needs to be created on the heap
      for(size_t ix=0; ix < payLoad; ix++) dummy[ix] = 0xAABB0000 + ix;
    }
    eformat::write::ROBFragment rob(src.code(), 
                                    l1r[0].rod_run_no(), 
                                    lvl1_id, 
                                    l1r[0].rod_bc_id(), 
                                    l1r[0].rod_lvl1_trigger_type(),
                                    event_type,
                                    payLoad,
                                    dummy,
                                    eformat::STATUS_FRONT);
    rob_size = rob.size_word();
    const eformat::write::node_t* rob_list = rob.bind();
    ERS_DEBUG(1, "Create HLT result ROB with size = " << rob_size << " 4-byte words @" << store);
    copy_size = eformat::write::copy(*rob_list, store, rob_size);
    delete[] dummy;
  }
  catch (ers::Issue& ex) { 
    throw pudummy::EformatFailure(ERS_HERE, "ers::Issue ", lvl1_id, ex);
  }
  catch (std::exception& ex) { 
    throw pudummy::EformatFailure(ERS_HERE, "std::exception ", lvl1_id, ex);
  }
  
  if (copy_size!=rob_size) {
      throw EformatCopyFailed(ERS_HERE, lvl1_id, copy_size, rob_size);
  }
  return rob_size;
}

/************************************************/
unsigned int pudummy::Steering::fill_hlt_result(
                const std::vector<eformat::read::ROBFragment >& l1r,
                HLTResult& hltr) const
{ 
  unsigned int rob_size = buildROBFragment(l1r, hltr.fragment_pointer.get());
  fill_trig_infos(hltr.trigger_info);
  return rob_size;
}

/************************************************/
void pudummy::Steering::fill_trig_infos(std::vector<uint32_t>& triggerInfos) const
{
  uint32_t n = config()->nbrHltTrigInfo();
  for(uint32_t i=0; i < n; ++i) {
    triggerInfos.push_back(0XEF00DE00 + i);
  }
}

/************************************************/
pudummy::Steering::result_t 
    pudummy::Steering::run_alg(const pudummy::DummyStep * alg,
                               uint64_t global_id,
                               std::vector<hltinterface::DCM_ROBInfo>& data,
                               const std::vector<eformat::read::ROBFragment >& l1r,
                               HLTResult& hltr)
{
  const DummyROBStep * lvl2_step = dynamic_cast<const DummyROBStep*>(alg);
  size_t initial_robs = data.size(); 
  if (m_dc) {
    if (lvl2_step)
    {
      collect(lvl2_step, global_id, data);
      m_is_helper.lvl2_steps++;
    } else {
      m_dc->collect(data, global_id);
      m_is_helper.ef_steps++;
    }
  } else {
    throw NoDataCollector(ERS_HERE);
  }
  unsigned int bt_us = alg->get_burn_time();
  burn_time(bt_us); 
  ERS_DEBUG(1, "GLOBALID " << global_id << " step " << alg->get_uid() << " collected " 
    << data.size()-initial_robs << " additional ROBs and burned " 
    << (bt_us+500)/1000 << " milli seconds cpu time.");
  return partial_fill_result(alg, global_id, l1r, hltr);
}

/************************************************/
void pudummy::Steering::collect(const pudummy::DummyROBStep * alg,
        uint64_t global_id,
        std::vector<hltinterface::DCM_ROBInfo>& data)
{
  const std::vector<DataAccess*> & roi = alg->get_roi();
  // Config::select_robs() reserves space but does not clear the vector ids.
  std::vector<uint32_t> ids;
  for (std::vector<DataAccess*>::const_iterator it = roi.begin();
    it != roi.end(); it++) {
    config()->get_ROB_IDs(global_id, *(*it), ids, alg->get_uid());
  }
  if (!ids.size()) {
    ERS_DEBUG(2, "GLOBALID=" << global_id << " step " << alg->get_uid()
       << " collects 0 ROBs.");
    return; 
  } else {
    // Remove duplicate ROB IDs.
    size_t tmp = ids.size();
    std::sort(ids.begin(), ids.end());
    std::vector<uint32_t>::iterator it_dup = std::unique(ids.begin(), ids.end());
    ids.erase(it_dup, ids.end());
    if (tmp != ids.size()) {
      m_is_info->duplicateROBsSelected += tmp-ids.size();
      if(!config()->ignore_bad_rob_access()) ers::warning(DuplicateROBsSelected(ERS_HERE, tmp-ids.size()));
    }
  }
  
  if (m_dc) {
    m_dc->collect(data, global_id, ids);
  } else {
      throw NoDataCollector(ERS_HERE);
  }
}

/************************************************/
void pudummy::Steering::burn_time(unsigned int burn_time_us) const
{
  config()->cpu_burner().burn(burn_time_us);
}


/************************************************/
bool pudummy::Steering::prepareWorker(const boost::property_tree::ptree& args)
{
   ERS_DEBUG(0, "Dummy Steering prepareWorker called with ptree named='" << args.get_value<std::string>() << "'.");
  try {
    m_is_info = (monsvc::MonitoringService::instance().register_object("pudummy", new PuDummySteering(), true)); 
    m_is_helper.lvl2_steps = 0;
    m_is_helper.ef_steps = 0;
    m_is_helper.ROBsAcceptedEvent = 0;
    m_is_helper.ROBsRejectedEvent = 0;
    // run ISInfoReceiver
    run_is();
    
    // Re-seed random number generator if TDAQ_PUDUMMY_RAND_RESEED is defined.
    config()->reseed_gRandom();
  }
  catch (ers::Issue& ex) { 
    throw(pudummy::ConfigurationIssue(ERS_HERE, "ers::Issue during prepareWorker", "", ex));
  }
  catch (std::exception& ex) { 
    throw(pudummy::ConfigurationIssue(ERS_HERE, "std::exception during prepareWorker", "", ex));
  }
  return true;
}

/************************************************/
bool pudummy::Steering::finalizeWorker(const boost::property_tree::ptree& args)
{
  ERS_LOG("Dummy Steering finalizeWorker called with ptree named='" << args.get_value<std::string>() << "'.");
  logISInfo();
  
  // Log configuration to enable compare with initial configuration.
  config()->log_config();
  
  monsvc::MonitoringService::instance().remove_object("pudummy");
  if (m_ISInfoReceiver) {
    try {
      //m_ISInfoReceiver->unsubscribe(m_ISConfigSub, false);
      ERS_LOG("Unsubscribed to " << m_ISConfigSub 
          << " ISInfoReceiver for PuDummySteering reconfiguration.");
    }
    catch (ers::Issue& ex) {
      ers::error(pudummy::ISInfoReceiverError(ERS_HERE, 
        "ers::Issue when removing", m_ISConfigSub.c_str(), ex.what()));
    }
    catch (std::exception& ex) {
      ers::error(pudummy::ISInfoReceiverError(ERS_HERE, 
        "std::exception  when removing", m_ISConfigSub.c_str(), ex.what()));
    }
  }
  return true;
}
