#ifndef PUDUMMY_MACROS_H
#define PUDUMMY_MACROS_H
/**
 * @author <a href="mailto:Per.Werner@cern.ch">Per Werner</a>
 * @brief Macros used by the HLTMPPUDummy package.
 * $Id$
 */
#include <string>

// Print a hex number.
#define HEX(x) "0x" << std::hex << x << std::dec
  
// Returns "TRUE" or "FALSE" as a string, depending on the boolean input.
inline std::string bool2str(bool b) {
  if (b) return "TRUE";
  return "FALSE";
}

#endif //PUDUMMY_MACROS_H
