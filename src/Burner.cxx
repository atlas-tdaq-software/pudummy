/*
 * @brief Implementations of the CPU burner for the HLTMPPU.
 * $Id$
 */
 
#include "ers/ers.h"
#include "Burner.h"

#include <cmath>

/**
 * A function that calculates nothing but uses CPU time.
 */
static double dummy(double value)  {
  value = (1 - value)/(1 + value);
  if (value < 0.001) value = value * 100.0;
  else value = value * 2.001;
  return value;
}

/**
 * The double value to use for our calculations
 */
static const double START_VALUE=0.5;

double pudummy::Burner::burn(unsigned int micro_seconds) const { 
  unsigned int total_loops = m_loopsPerUsec * micro_seconds;
  ERS_DEBUG(3, "Burning " << micro_seconds << " usec(s) (" << total_loops 
	    << " cycles).");
  volatile double value = START_VALUE;
  for (unsigned int i=0; i<total_loops; i++) value = dummy(value);
  return value;
}

pudummy::Burner::Burner() 
: m_loopsPerUsec(0)
{
  // Calibrate the CPU Burner.
  /* Determines how many loops we need to execute microsecond.
   */
  static const unsigned int LOOPS=10000000;
  std::chrono::high_resolution_clock clock;
  unsigned int loops = LOOPS;
  volatile double value = START_VALUE;
  double loopsPerUsec = 0.0;
  for(size_t j=0; j<10; j++) {
    
    auto begin = clock.now();
    for (unsigned int i=0; i<loops; i++) value = dummy(value);
    auto end = clock.now() - begin;

    if (loopsPerUsec == 0.0) //first assignment
      loopsPerUsec = loops/std::chrono::duration_cast<std::chrono::microseconds>(end).count();
    else { //other assignments, migrate towards a mean value
      m_loopsPerUsec = m_loopsPerUsec+(loops/std::chrono::duration_cast<std::chrono::microseconds>(end).count())/2;
    }
    ERS_DEBUG(3, "CPU burner calibration " << j << " with loops=" << loops
        << " to " << loopsPerUsec << "/usec.");
    loops=loops/2;
  }
  m_loopsPerUsec = static_cast<unsigned int>(loopsPerUsec + 0.5);
  ERS_LOG("CPU burner calibrated to " << m_loopsPerUsec
	    << " cycles per usec for this computer.");
}
  
pudummy::Burner::~Burner(){
}
    
