/**
 * @author <a href="mailto:Per.Werner@cern.ch>Per Werner PH-ADT CERN</a>
 * $Id$
 * @brief Configuration for the dummy implementation of HLTMPPU.
 */

/* Note: 2013-02-07/Per Werner
 * After the changed ptree structure only alorithm steps 
 * are unique. Each step contain it's private sub components, 
 * e.g., if a StreamTag or DataAccess object is used several times
 * there are multiple instancies of these objects.
 *
 * This waists some memory but was considered to be a simpler
 * implementation when the previous configuration with all 
 * objects unique had to be updated to the new ptree structure.
 */
#ifndef PUDUMMY_CONFIG_H
#define PUDUMMY_CONFIG_H

#include <atomic>
#include <vector>
#include <stdint.h>
#include <boost/property_tree/ptree.hpp>

#include "dccommon/Distribution.h"
#include "Burner.h"

#include "eformat/eformat.h"

class ISCallbackInfo; //Forward

namespace pudummy {
  /**
   * The DataAccess describes the ROBs needed from one
   * SubDetector[Group] or all available SubDetectors.<br>
   * The 'master' description of the 'DataAccess' OKS class 
   * can be found in the schema documentation.
   * <br>
   * An object of the DataAccess class determines the set of ROBs selected
   * from a subdetector, subdetectorGroup or all available subdetectors.
   * <ol>
   * <li> If the subDetector attribute is non-zero,  
   * the ROBs are selected from the specifed 
   * subDetectorGroup or subDetector. 
   * A value of zero(eformat::FULL_SD_EVENT) has the effect that ROBs 
   * are selected from all available SubDetectors.
   * </li>
   * <li> The 'nROBDistribution' attribute determines the number of ROB's to be requested
   * for this data access. The value for nROBs can be configured to be constant, 
   * a Root function or a Root histogram, see class dccommon::Distribution.
   * If nROBDistribution evaluates to zero and Subdetector is set,
   * all ROBs in the Subdetector[Group] are selected.Config
   * </li>
   * </ol>
   * Note that if both nROBDistribution and Subdetector evaluates to zero,
   * no ROBs are selected.
   */  
  class DataAccess {
    public:
    DataAccess(const std::string & uid, unsigned int sd, const std::string & nROBDistributionString);
    ~DataAccess();
    inline const std::string& get_uid() const { return m_uid; }
    inline unsigned int get_subDetector() const { return m_subDetector; }
    inline const std::string & get_nROBDistributionString() const { return m_nROBDistribution->get_DistributionString(); }
    unsigned int get_nROBs() const;
    
    private:
    const std::string m_uid;              ///< UID of the OKS DataAccess object.
    unsigned int m_subDetector;           ///< The code for the SubDetector or subDetectorGroup.
    DC::Distribution* m_nROBDistribution; ///< The ROB distribution object.
  };
  
  /**
   * This represents the configuration of one stream tag that can be applied
   * to an event
   */
  class DummyCalStreamTag; // Forward
  class DummyStreamTag
  {
    public:
    DummyStreamTag(const std::string & uid, float probability);
    virtual ~DummyStreamTag();
    inline const std::string& get_uid() const { return m_uid;}
    inline float get_probability() const { return m_probability;}
    inline void set_probability(float probability) { m_probability = probability;}
    
    /**
     * Add a DummyStreamTag from the ptree.
     *
     * @param tagName 'debugTag' or 'physicsTag'. For messaging only.
     * @param tree The ptree with the DummyStreamTag to add.
     * @return A pointer to the created DummyStreamTag.
     */
    static pudummy::DummyStreamTag * addStreamTag(const char* tagName, const boost::property_tree::ptree & ptree);
            
    private:
    std::string m_uid;
    float m_probability; // Atomic to make lock free update from IS.
    friend class DummyCalStreamTag;
    friend class Config;
  };
    
  class Config; //Forward decalation.
  /**
   * This represents the configuration for a calibration stream tag that can
   * be applied to an event, including the number of ROBs to be used for 
   * partial event building
   */
  class DummyCalStreamTag : public DummyStreamTag 
  {
    public:
    DummyCalStreamTag(const std::string & uid, float probability, pudummy::DataAccess* da);
    virtual ~DummyCalStreamTag();
    bool set_calibData(const Config & config, const std::vector<std::string> & dataAccess_names);
    const DataAccess * get_calibrationData() const;

      /**
     * Add a DummyCalStreamTag from the ptree.
     * @param ptree The ptree with the DummyCalStreamTag to add.
     * @return A pointer to the created DummyCalStreamTag.
     */
    static DummyCalStreamTag * addCalStreamTag(const boost::property_tree::ptree & ptree);   
    private:
    DataAccess* m_calibrationData;
  };
    
  /**
   * This represents a general algorithm
   */
  class DummyStep
  {
    public:    
    DummyStep(const std::string & uid, 
        const std::string & burn_time_distribution, 
        DummyStreamTag * physics_tag, 
        DummyStreamTag * debug_tag, 
        DummyCalStreamTag * calibration_tag);
    virtual ~DummyStep();
    
    inline const std::string& get_uid() const { return m_uid; }
    
    /**
     * Burn time in micro seconds for this step.
     */
    unsigned int get_burn_time() const;
    inline const std::string get_burn_time_string() const { return m_burn_time->get_DistributionString(); }
    const std::string& get_burn_time_type() const;
    const DummyStreamTag& get_physics_tag() const;
    const DummyStreamTag& get_debug_tag() const;
    const DummyCalStreamTag* get_calibration_tag() const;
  
    private:
    DummyStep(const DummyStep&);
    
    const std::string        m_uid;
    DC::Distribution       * m_burn_time;
    const DummyStreamTag   * m_physics_tag;
    const DummyStreamTag   * m_debug_tag;
    const DummyCalStreamTag* m_calibration_tag;
  };
    
  /**
   * This represents an algorithm that asks for an RoI, a subset of all ROBs
   */
  struct DummyROBStep : public DummyStep
  {
    public:
    DummyROBStep(const std::string & uid, 
        const std::string & burn_time_distribution, 
        DummyStreamTag * debug_tag, 
        DummyStreamTag * physics_tag, 
        DummyCalStreamTag * calibration_tag, 
        std::vector<DataAccess*> roi);
    
    virtual ~DummyROBStep();
    
    /**
     * Get the data descriptor for this RoI.
     * @return a vector of pointers to DataAccess objects.
     */
    inline const std::vector<DataAccess*> & get_roi() const { return m_roi; } 
  
    private:       
    std::vector<DataAccess*> m_roi;
  };

  /**
   * Defines the HLTMPPU dummy steering configuration. This class is thread-safe.
   */
  class Config {
    public:
    /** 
     * Create a PuDummySteering, 
     * The PuDummySteering object and all related objects are defined in the ptree.
     * <br>An exception can be thrown by underlying SW and is caught, 
     *  then re-thrown to give execution context.
     * 
     * <br> This constructor finds the PuDummySteering and all nested objects. 
     * All objects contained are created as the ptree is parsed.
     * <br> Note that this way multiple instancies of the same OKS object can be created.
     * This simplifies creation and destruction at the cost of a small amount of memory.
     *
     * @param args The ptree defining the configuration for this dummy steering.
     */
    Config(const boost::property_tree::ptree & args); 
    
    /**
     * Destructor
     * 
     * Frees the space that was created for the algorithms
     */
    ~Config();

    /**
     * Returns the size in 32 bit words to be used for the generated result.
     */
    uint32_t result_size() const;

    uint32_t nbrHltTrigInfo() const;
    uint32_t nbrPscErrors()  const;

    /**
     * Returns the status of the steering configuration. 
     * @return <code>false</code> The steering 
     * configuration failed. Steering cannot be executed.
    */
    bool status(void) const;

    /**
     * @return The CPU burner that will be used by the algorithms.
     */
    const Burner& cpu_burner(void) const;
    
    /**
     * @return The set of algorithms that can be run for each event.
     * <br>We expect all "DummyROBStep's" (LVL2) to be defined  
     * before any DummyStep (EF) is defined.
     */
    const std::vector<DummyStep*>& algorithms() const;
    
    /**
     * Select 'asked_robs' random ROB identifiers from the set of  
     * configured and enabled ROB identifiers.
     *
     * @warning Some of the identifiers may not be present for 
     * simulated events. The ROS is then expected to return an 
     * empty ROB fragment.
     *
     * @param lvl1_id The LVL1 ID for the event.
     * @param asked_robs The number randomly selected ROBs to return.
     * @param sd The SubDetector[Group] of the ROBs wanted. 
     * If sd is zero ROBs from the full event (all sub detectors) are selected. 
     * @param ids Vector with the selected ROB identifiers provided by the caller.
     * 
     * <b>Note</b> that when subdetector zero is selected, no ROBs from 
     * the 'virtual' MET subdetectors are selected. 
     */
    void select_robs(uint32_t lvl1_id, uint32_t nrobs, unsigned int sd,
		  std::vector<uint32_t>& ids);
    
    /**
     * @return The ptree with the full configuration.
     */
    inline const boost::property_tree::ptree & config_tree() const { return m_config_tree; } 
        
    /**
     * Function to get the ROB IDs for one DataAccess object, i.e., 
     * part of a RoI or calibration tag.
     *
     * @param lvl1_id The LVL1 ID for this event.
     * @param dataAccess The object describing the ROB IDs to access.
     * @param ids The selected ROB IDs. Note that this vector is not cleared by successive calls for the same algorithm.
     * @param alg_name The algorithm (step) that asks for these ROB IDs.
     */
    void get_ROB_IDs(uint32_t lvl1_id, 
                const pudummy::DataAccess & dataAccess, 
                std::vector<uint32_t> & ids,
                const std::string & alg_name);
    
    /**
     * If true, don't give ERS warnings if 
     * <ol>
     * <li> the subdetector is empty.</li>
     * <li> there are not enough ROBs for the request.</li>
     * <li> Duplicate ROBs are selected.</li>
     * </ol>
     * Note that this info can be found in the pudummy IS object.
     */
    inline bool ignore_bad_rob_access() { return m_ignore_bad_rob_access; }
    
   /**
     * Returns the parameter that defines how ROOTs gRandom function
     * was last seeded. The initial value is defined in the 
     * OKS attribute "PuDummySteering.seed".
     * If the variable TDAQ_PUDUMMY_RAND_RESEED is defined 
     * re-seeding is done in prepareWorker.
     * <br>If s_seed is
     * -# > 0 
     *   -# when passed to the Distribution constructur the value of s_seed  
     *   -# when used to re-seed, the value from std::random_device  
     * -# == 0, the current value of the high resolution clock 
     * -# < 0 the process ID
     * 
     * is used to seed the random number generator   
     * defined by ROOTs gRandom variable. 
     */
    static inline int32_t seed(void) { return s_seed; }
    
    /**
     * Reseed ROOTs gRandom function if the variable
     * TDAQ_PUDUMMY_RAND_RESEED is defined.
     */
    void reseed_gRandom();
    
    /**
     * Print the current pudummy configuration.
     */
    void log_config() const; 
        
    private:
    std::string        m_uid;  ///< The name of the Dummy steering
    const boost::property_tree::ptree & m_config_tree; 
    DC::Distribution * m_result_size;   ///< The size to use for the result distribution
    uint32_t           m_nbrHltTrigInfo; ///< Nbr EFTriggerInfo words beside the "daq" one (1st)
    uint32_t           m_nbrPscErrors;  ///< Nbr simulated psc error words
    bool               m_status;        ///< defines if the configuration has succeeded or not
    Burner             m_burner;        ///< the CPU burner to use
    std::vector<DummyStep*> m_algorithms;    ///< The algorithms to run
    
    static int32_t        s_seed;       ///< How to re-seed ROOTs gRandom function.
    static unsigned int s_reseed_value; ///< The seed for ROOTs gRandom. 
    
    std::vector<uint32_t> m_all_robs; ///< All configured ROBs are defined as a std::set of 32 bit ROB IDs.
    std::map<unsigned int, std::vector<uint32_t> > m_subdet_robs; ///< A map with subDetector[Group] as key and a vecor of ROB IDs as value.
    
    bool m_ignore_bad_rob_access; ///< Don't give ERS warnings for empty subdetector & not enough ROBs if true.
     
    /**
     * Variables to monitor error conditions in the Config class.
     * Used be the Steering class via the access functions.
     */
    uint64_t m_notEnoughROBs; ///< Number of ROBs not available for a RoI or Calibration data request
    uint64_t m_emptySubdetectorAccess; ///< Number of accesses to an empty subdetector
    public:
    inline uint64_t notEnoughROBs() { return m_notEnoughROBs; }
    inline uint64_t emptySubdetectorAccess() { return m_emptySubdetectorAccess; }
  };
}

/************************************************/
inline uint32_t pudummy::Config::nbrHltTrigInfo() const
{
  return m_nbrHltTrigInfo;
}

/************************************************/
inline uint32_t pudummy::Config::nbrPscErrors() const
{
  return m_nbrPscErrors;
}

/************************************************/
inline bool pudummy::Config::status() const
{
  return m_status;
}

/************************************************/
inline const pudummy::Burner& pudummy::Config::cpu_burner() const
{
  return m_burner;
}

/************************************************/
inline const std::vector<pudummy::DummyStep*>& 
pudummy::Config::algorithms() const
{
  return m_algorithms;
}

/************************************************/
inline const pudummy::DataAccess * pudummy::DummyCalStreamTag::get_calibrationData() const
{
  return m_calibrationData;
}

/************************************************/
inline const pudummy::DummyStreamTag& pudummy::DummyStep::
get_physics_tag() const
{
  return *m_physics_tag;
}

/************************************************/
inline const pudummy::DummyStreamTag& pudummy::DummyStep::
get_debug_tag() const
{
  return *m_debug_tag;
}

/************************************************/
inline const pudummy::DummyCalStreamTag* pudummy::DummyStep::
get_calibration_tag() const
{
  return m_calibration_tag;
}

/************************************************/
inline const std::string& pudummy::DummyStep::
get_burn_time_type() const
{
  return DC::Distribution::DistributionType2string(m_burn_time->distributionType());
}

#endif /*PUDUMMY_CONFIG_H*/
