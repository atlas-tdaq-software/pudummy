#ifndef PUDUMMY_STEERING_H
#define PUDUMMY_STEERING_H

/**
 * @brief The MPPU dummy steering implementation .
 * Based on previous dummy steering versions by Ricardo.Abreu@cern.ch, 
 * Andrea.Negri@pv.infn.it and Per.Werner@cern.ch.
 * <br>
 * $Id$
 */

#include <chrono>
#include <atomic>
#include <boost/thread/mutex.hpp>

#include "Config.h"
#include "hltinterface/HLTInterface.h"
#include "hltinterface/DCM_ROBInfo.h"
#include "eformat/eformat.h"
#include "monsvc/ptr.h"
#include "monsvc/MonitoringService.h"
#include "is/inforeceiver.h"

#include "pudummy/PuDummySteering.h"

namespace hltinterface {
  class DataCollector; //Forward declaration
}

namespace pudummy {

  class HLTResult;
  
  /** 
   Class holding data needed to compute some 
   PuDummySteering (IS object) attributes. 
  */

  class ISHelper { 
    public:
      ISHelper(); 
      ~ISHelper(); 
      
      // public data for now ??
      uint64_t lvl2_steps;
      uint64_t ef_steps;
      uint64_t ROBsAcceptedEvent;
      uint64_t ROBsRejectedEvent;
      // If not enough info from DCM/MPPU, add some timing related info with std::chrono::high_resolution_clock:
      //   interval_time(timer wakeup?), start_time, last_update_time, event_rate, data_rate.
  }; 
     
  /**
   * Defines a dummy steering that can emulate a simplified 
   * PESA Steering processing. 
   * This Steering controls a set of dummy algorithms that
   * perform the validation work for a given event. 
   * The object has a default way of being built. 
   * Internally, the steering executes algorithm steps until
   * <ol> 
   *  <li> a step rejects the event.</li>
   *  <li> all steps accepts the event.</li>
   * </ol>  
   * Each step access ROB data and can add calibration data  
   * to the intermediate result.
   * <br>A dummy HLT result is generated if the event is accepted.
   * 
   * <br>@TODO The handling of the debug tags should be verified. 
   * I assume setting a debug tag should stop processing and make 
   * the event go to the DEBUG stream in question.
   * <br> Now the event is simply rejected.
   */
  class Steering : public hltinterface::HLTInterface
  {
  public: 
    /**
     * Internal processing status.
     */ 
    typedef enum {ACCEPT, REJECT, DEBUG, FAILURE} result_t; 
    
    /**
     * C'tor.
     */
    Steering();

    /**
     * D'tor virtualisation
     */
    virtual ~Steering() {}

    /**
     * Configures the framework
     *
     * @param args is a property tree containing 
     * the configuration parameters.
     * system.
     */
    virtual bool configure(const boost::property_tree::ptree& args) override;
                            
    virtual bool connect(const boost::property_tree::ptree& args) override;
    virtual bool prepareForRun(const boost::property_tree::ptree& args) override;
    virtual bool stopRun(const boost::property_tree::ptree& args) override;
    virtual bool disconnect(const boost::property_tree::ptree& args) override;
    virtual bool unconfigure(const boost::property_tree::ptree& args) override;
    virtual bool prepareWorker(const boost::property_tree::ptree& args) override;
    virtual bool finalizeWorker(const boost::property_tree::ptree& args) override;

    virtual bool doEventLoop() override;

    /**
     * Calls the HLT framework to notify it that a user command has arrived
     * @param args Boost property tree where this function can find arguments.
     */
    virtual bool hltUserCommand(const boost::property_tree::ptree& args) override;
  
    /**
     * Calls the HLT framework to publish statistics, after the run has
     * finished.
     * @param args Boost property tree where this function can find arguments.
     */
    virtual bool publishStatistics(const boost::property_tree::ptree& args) override;

    /**
     * Calls the HLT framework to notify it that a time out is imminent.
     * @param args Boost property tree where this function can find arguments.
     */
    virtual void timeOutReached(const boost::property_tree::ptree&);
         
  protected:    

    /**
     * Process one event taking as input the LVL1 Result and a 
     * memory area to write the HLT result. 
     * <br> The output produced:
     *  - a new HLT ROB (serialized on the provided buffer)
     *  - the vector of stream tags (vector of eformat::StreamTag objects)
     *  - the vector of HltTriggerInfo  
     *  - the vector of ROB IDs to be potentially used for event stripping (calibration). 
     *    Note a SubDetector or SubDetectorGroup is converted to a vector of ROB IDs.
     *  - the vector containing possible PSC errors
     *
     * @param l1r The LVL1 result ROB's
     * @param hltr The HLT result. 
     */
    bool process(const std::vector<eformat::read::ROBFragment >& l1r,
                 uint64_t global_id,
                 pudummy::HLTResult& hltr);

    /// build ROB fragment
    unsigned int buildROBFragment(const std::vector<eformat::read::ROBFragment >& l1r, uint32_t* store) const;
    
    unsigned int fill_hlt_result(const std::vector<eformat::read::ROBFragment >& l1r,
                                 pudummy::HLTResult& hltr) const;
             
    result_t partial_fill_result(const pudummy::DummyStep * alg,
                                 uint64_t global_id,
                                 const std::vector<eformat::read::ROBFragment >& l1r,
                                 pudummy::HLTResult& hltr);
                                                      
    /// Fill triggerInfos with info words
    void fill_trig_infos(std::vector<uint32_t>& triggerInfos) const;
               
    /**
     * Run algorithm alg for an algorithm step for an event.
     * Fill in data with ROBs and ids with the respective IDs.
     * @params alg Pointer to the algorithm step.
     * @params lvl1_id LVL1 ID for the event.
     * @params data The collected ROBs with cost monitoring info, see hltinterface/DCM_ROBInfo.h.
     * @params l1r The list of L1 results
     * @return the processing status.
     */
    result_t run_alg(const DummyStep * alg, 
                     uint64_t global_id,
                     std::vector<hltinterface::DCM_ROBInfo>& data,
                     const std::vector<eformat::read::ROBFragment >& l1r,
                     HLTResult& hltr);
               
    /**
     * Collect ROI data for this algorithm.
     * @param alg The algorithm (step) to execute.
     * @param lvl1_id The LVL1 ID for this event..
     * @params data The collected ROBs with cost monitoring info, see hltinterface/DCM_ROBInfo.h.
     */
    void collect(const DummyROBStep * alg,
                 uint64_t global_id,
                 std::vector<hltinterface::DCM_ROBInfo>& data);
             
    /// Burn bt micro seconds for lvl1_id
    void burn_time(unsigned int bt) const;
    
    void logISInfo();
 
    /// ISInfoReceiver helper function
    void run_is();
   
    inline Config* config() const { return s_config[s_activeConfig]; }
   
  private: ///< representation
#define PUDUMMY_NUM_CONFIGS 2
    static Config   * s_config[PUDUMMY_NUM_CONFIGS]; ///< my configurations
    static size_t     s_activeConfig;              ///< index of my active configuration
    static boost::mutex s_config_mutex;            ///< Lock against concurrent reconfigurations
    std::string m_ISConfigSub;              ///< IS subscription string for configuration change
    int        m_runNumber;                 ///< run number to be used for this run
    hltinterface::DataCollector * m_dc;     ///< The DataCollector used to get robs
    bool       m_timeOut;                   ///< Set when processing timeout is near.
    monsvc::ptr<PuDummySteering> m_is_info; ///< IS object for publishing statistics for pudummy
    /** Variables needed to compute some PuDummySteering attributes. */
    ISHelper m_is_helper;
    std::string m_tdaq_is_server;          ///< IS server used for reconfiguration 
    ISInfoReceiver* m_ISInfoReceiver;
    std::atomic<bool> m_running;
    static void reconfig_IS_callback(ISCallbackInfo * icbi);
  };
}
#endif /*PUDUMMY_STEERING_H*/
