//Dear emacs, this is -*- c++ -*-

/**
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a> 
 * or <a href="mailto:Per.Werner@cern.ch">Per Werner</a> 
 * $Id$
 *
 * @brief Creates an Interface object of the concrete 
 * type PUDUMMY::Steering.
 */

#ifndef PUDUMMY_HLT_FACTORY_H
#define PUDUMMY_HLT_FACTORY_H

#include "hltinterface/HLTInterface.h"

extern "C" {
hltinterface::HLTInterface* hlt_factory(void);
}

extern "C" {
void destroy_hlt_factory(hltinterface::HLTInterface*);
}

#endif /* PUDUMMY_HLT_FACTORY_H */
