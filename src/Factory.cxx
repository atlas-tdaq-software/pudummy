//Dear emacs, this is -*- c++ -*-

/**
 * @file hlt_factory.cxx
 * @author <a href="mailto:Andre.dos.Anjos@cern.ch">Andre DOS ANJOS</a> 
 * or <a href="mailto:Per.Werner@cern.ch">Per Werner</a> 
 * $Id$
 *
 * Implements the hlt_factory.
 */

#include "Factory.h"
#include "Steering.h"

hltinterface::HLTInterface* hlt_factory(void)
{
  return new pudummy::Steering();
}

void destroy_hlt_factory(hltinterface::HLTInterface* p)
{
  delete p;
}



