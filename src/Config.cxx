/*
 * @brief Configuration for the dummy implementation of HLTMPPU.
 * $Id$
 */
 
#include "macros.h"
#include "Issues.h"
#include "Config.h"
#include "is/infoT.h"
#include "is/infoiterator.h"
#include "is/callbackinfo.h"

#include <sys/types.h>
#include <unistd.h>
#include <chrono>
#include <random>

#include <boost/property_tree/ptree.hpp>
#include <boost/foreach.hpp>
#include <vector>
#include <boost/assign/std/vector.hpp>
#include <boost/algorithm/string.hpp>

#include "TRandom.h"

// static member initialisation
int32_t pudummy::Config::s_seed = 0; ///< Default seed random function is zero.
unsigned int pudummy::Config::s_reseed_value(0);

static const std::string DEFAULT_VALUE(""); // Used as default value for ptree access
static const boost::property_tree::ptree SELF_TYPE; // Used as default type for ptree access

static void streamPtree(std::ostringstream & oss, const boost::property_tree::ptree& args, std::string level)
{
  boost::property_tree::ptree::const_iterator it,itend=args.end();
  level+=" ";
  for(it=args.begin();it!=itend;++it){
    oss<<level<<it->first<<" : "<<it->second.get_value<std::string>()<<std::endl;
    streamPtree(oss, it->second, level);
  }
}

/************************************************/
pudummy::DataAccess::DataAccess(const std::string & uid, unsigned int sd, const std::string & nROBDistributionString)
 : m_uid(uid),
   m_subDetector(sd)
{ 
  m_nROBDistribution = new DC::Distribution(nROBDistributionString, Config::seed());
  ERS_DEBUG(0, "Created DataAccess object=" << m_uid << " for subdector=" << HEX(m_subDetector)
    << " with ROBDistributionString='" << get_nROBDistributionString() 
    << "' and seed parameter=" << Config::seed());
}
/************************************************/
pudummy::DataAccess::~DataAccess() {
  delete m_nROBDistribution;
  m_nROBDistribution = 0;
}

/************************************************/
unsigned int pudummy::DataAccess::get_nROBs() const {
  return m_nROBDistribution->next_value();
}

/************************************************/
pudummy::DummyStreamTag::DummyStreamTag(const std::string & uid, float probability)
 : m_uid(uid),
   m_probability(probability)
{
}

/************************************************/
pudummy::DummyStreamTag::~DummyStreamTag() {
}

/************************************************/
pudummy::DummyCalStreamTag::DummyCalStreamTag(const std::string & uid, 
    float probability, pudummy::DataAccess* da)
 : DummyStreamTag(uid, probability),
   m_calibrationData(da)
{
  ERS_DEBUG(0, "Construct DummyCalStreamTag=" << uid 
    << " and DataAccess pointer=" << da);
}

/************************************************/
pudummy::DummyCalStreamTag::~DummyCalStreamTag() 
{
  delete m_calibrationData;
  m_calibrationData = 0;
}

/************************************************/
pudummy::DummyStep::DummyStep(const std::string & uid, 
    const std::string & burn_time_distribution, 
    DummyStreamTag * debug_tag, 
    DummyStreamTag * physics_tag, 
    DummyCalStreamTag * calibration_tag)
  : m_uid(uid),
    m_physics_tag(physics_tag),
    m_debug_tag(debug_tag),
    m_calibration_tag(calibration_tag)
{
  m_burn_time = new DC::Distribution(burn_time_distribution, Config::seed());
  ERS_DEBUG(0, "Constructor for DummyStep=" << m_uid << ", burn_time_distribution="
      << m_burn_time->get_DistributionString() << ", debug_tag=" << m_debug_tag->get_uid()
      << ", physics_tag=" << m_physics_tag->get_uid()
      << ", calibration_tag=" 
      << (m_calibration_tag ? m_calibration_tag->get_uid() : "NotDefined")
      << ".");
}

/************************************************/
pudummy::DummyStep::~DummyStep()
{
  /* As stream tags may be shared they must be 
   * deleted at the end of the Config destructor.
   */
  m_physics_tag = 0;
  m_debug_tag = 0;
  m_calibration_tag = 0;
  delete m_burn_time; m_burn_time = 0;   
  delete m_physics_tag; m_physics_tag = 0;
  delete m_debug_tag; m_debug_tag = 0;
  delete m_calibration_tag; m_calibration_tag = 0;
}

/************************************************/
unsigned int pudummy::DummyStep::get_burn_time() const
{
  return m_burn_time->next_value();
}

/************************************************/
pudummy::DummyROBStep::DummyROBStep(const std::string & uid, 
    const std::string & burn_time_distribution, 
    DummyStreamTag * debug_tag, 
    DummyStreamTag * physics_tag, 
    DummyCalStreamTag * calibration_tag,
    std::vector<DataAccess*> roi)
  : DummyStep(uid, burn_time_distribution, debug_tag, physics_tag, calibration_tag)
{
  ERS_DEBUG(0, "Constructor for DummyROBStep=" << uid << " with " << roi.size() << " Data accesses.");
  for (std::vector<DataAccess*>::iterator it = roi.begin(); it != roi.end(); it++) {
    m_roi.push_back(*it);
  }
}

pudummy::DummyROBStep::~DummyROBStep()
{
  /* As stream tags are not shared they can be 
   * deleted here.
   */
  for (std::vector<DataAccess*>::iterator it = m_roi.begin();
      it != m_roi.begin(); it++) {
    delete *it; 
    *it =0;
  }
  m_roi.clear();
}
/************************************************/
// Helper function to Config constuctor that finds parameter in a given ptree.
static bool get_ptree_params(const std::pair<const std::string, 
    const boost::property_tree::ptree> pair,
    std::vector<std::string> & params,
    std::map<std::string, std::string> & result)
{
  bool status = true;
  for (std::vector<std::string>::const_iterator itp = params.begin();
      itp != params.end(); itp++) {
    std::string value = 
        pair.second.get(
        (*itp), 
        DEFAULT_VALUE);
    if (value == DEFAULT_VALUE) {
      std::ostringstream oss; 
      streamPtree(oss, pair.second, "");
      ers::error(pudummy::AttributeNotFound(ERS_HERE, (*itp).c_str(), pair.first.c_str(), "", "", oss.str().c_str()));
      status = false;
    }
    result[(*itp)] = value;
  }
  return status;
}

/************************************************/
static void streamTagParams(const boost::property_tree::ptree & tagTree, std::string & uid, float & probability)
{   
  uid = tagTree.get<std::string>("UID");
  const std::string probString = tagTree.get<std::string>("probability");
  errno=0;
  probability = strtod(probString.c_str(), 0);
  if (errno) {
    std::ostringstream oss;
    oss << "Error='" << strerror(errno) << "' when converting the probability '" << probString << "' for Stream tag=";
    throw(pudummy::ConfigurationIssue(ERS_HERE, oss.str().c_str(), uid.c_str()));
  }
}

/************************************************/
/**
 * Create a DataAccess object from the ptree.
 * @param parentUID The OKS UID of parent object. For messaging only.
 * @param tree The ptree with the DataAccess definition.
 * @return A pointer to the created DataAccess object.
 */
static pudummy::DataAccess * addDataAccess(const char* parentUID, 
    const boost::property_tree::ptree& ptree)
{
  try {
    const std::string uid = ptree.get<std::string>("UID");
    const std::string subDetectorString = ptree.get<std::string>("subDetector");
    unsigned int subDetector;
    const std::string nROBDistribution = ptree.get<std::string>("nROBDistribution");
    errno=0;
    subDetector = strtoul(subDetectorString.c_str(), 0, 0);
    if (errno) {
      std::ostringstream oss;
      oss << "Error='" << strerror(errno) << "' when converting the sub detector ID=";
      throw(pudummy::ConfigurationIssue(ERS_HERE, oss.str().c_str(), subDetectorString.c_str()));
    }
    return new pudummy::DataAccess(uid, subDetector, nROBDistribution);
  }
  catch (ers::Issue& ex) { 
    throw(pudummy::ConfigurationIssue(ERS_HERE, 
      "ers::Issue when creating DataAccess object=", parentUID, ex));
  }
  catch (std::exception& ex) { 
    throw(pudummy::ConfigurationIssue(ERS_HERE, 
      "std::exception when creating DataAccess object=", parentUID, ex));
  }
}

/************************************************/
/**
 * Create a DummyCalStreamTag from the ptree.
 * @param tree The ptree with the DummyCalStreamTag to add.
 * @return A pointer to the created DummyCalStreamTag.
 */
pudummy::DummyCalStreamTag * pudummy::DummyCalStreamTag::addCalStreamTag(const boost::property_tree::ptree & ptree)
{ 
  std::string uid("");
  try {
    const boost::property_tree::ptree & tagTree = ptree.get_child("DummyCalStreamTag", SELF_TYPE);
    if (tagTree == SELF_TYPE) {
      std::ostringstream oss; 
      streamPtree(oss, ptree, "");
      throw(pudummy::AttributeNotFound(ERS_HERE, "calibrationTag", "DummyCalStreamTag", "", "", oss.str().c_str()));
    }
    float probability;
    streamTagParams(tagTree, uid, probability);

    // Get DataAccess object 
    const boost::property_tree::ptree & dataAccessTree = tagTree.get_child("calibData.DataAccess", SELF_TYPE);
    if (dataAccessTree == SELF_TYPE) {
      std::ostringstream oss; 
      streamPtree(oss, tagTree, "");
      throw(pudummy::AttributeNotFound(ERS_HERE, "calibData.DataAccess", "DummyCalStreamTag", "=", uid.c_str(), oss.str().c_str()));
    }
    return new pudummy::DummyCalStreamTag(uid, probability, addDataAccess(uid.c_str(), dataAccessTree));
  }
  catch (ers::Issue& ex) { 
    throw(pudummy::ConfigurationIssue(ERS_HERE, "ers::Issue when creating a DummyCalStreamTag=", uid.c_str(), ex));
  }
  catch (std::exception& ex) { 
    throw(pudummy::ConfigurationIssue(ERS_HERE, "std::exception when creating a DummyCalStreamTag=", uid.c_str(), ex));
  }
}

/************************************************/
/**
 * Create a DummyStreamTag from the ptree.
 * @param tagName 'debugTag' or 'physicsTag'. For messaging only.
 * @param tree The ptree with the DummyStreamTag to add.
 * @return A pointer to the created DummyStreamTag.
 */
pudummy::DummyStreamTag * pudummy::DummyStreamTag::addStreamTag(const char* tagName, const boost::property_tree::ptree & ptree)
{ 
  const boost::property_tree::ptree & tagTree = ptree.get_child("DummyStreamTag", SELF_TYPE);
  if (tagTree == SELF_TYPE) {
    std::ostringstream oss; 
    streamPtree(oss, ptree, "");
    throw(pudummy::AttributeNotFound(ERS_HERE, tagName, "DummyStreamTag", "", "", oss.str().c_str()));
  }
  std::string uid;
  float probability;
  streamTagParams(tagTree, uid, probability);
  return new DummyStreamTag(uid, probability);
}

/************************************************/
/**
 * Create an RoI for a DummyROBStep from the ptree.
 * @param robStep is the UID of the DummyROBStep. For messaging only.
 * @param ptree The ptree with the RoI to create.
 * @param roi The vector to store the RoI.
 */
static void addRoi(const char * robStep, const boost::property_tree::ptree& ptree, std::vector<pudummy::DataAccess*> & roi) 
{
  
  try {
    boost::property_tree::ptree::const_iterator it;
    for (it = ptree.begin(); it != ptree.end(); it++) {
      ERS_DEBUG(0, "addRoi to robStep=" << robStep);
      roi.push_back(addDataAccess(robStep, (*it).second));
    }
  }
  catch (ers::Issue& ex) { 
    throw(pudummy::ConfigurationIssue(ERS_HERE, "ers::Issue when creating RoI for DummyROBStep=", robStep, ex));
  }
  catch (std::exception& ex) { 
    throw(pudummy::ConfigurationIssue(ERS_HERE, "std::exception when creating RoI for DummyROBStep=", robStep, ex));
  }
}

/************************************************/
/**
 * Create an algorithm step from the ptree.
 * @param it An iterator for the algorithm step to add.
 * @return a pointer to the created algorithm step.
 */
static pudummy::DummyStep* addStep(boost::property_tree::ptree::const_iterator step_it)
{ std::string uid("");
  try {
    const boost::property_tree::ptree & step = (*step_it).second;
    boost::property_tree::ptree::const_iterator it;
    uid = step.get<std::string>("UID");
    const std::string btDist = step.get<std::string>("burnTimeDistribution");
    const boost::property_tree::ptree& debugTag_tree = step.get_child("debugTag", SELF_TYPE);
    if (debugTag_tree == SELF_TYPE) {
      std::ostringstream oss; 
      streamPtree(oss, step, "");
      throw(pudummy::AttributeNotFound(ERS_HERE, "debugTag", "DummyStreamTag", " for step ", uid.c_str(), oss.str().c_str()));
    }
    pudummy::DummyStreamTag * debugTag = pudummy::DummyStreamTag::addStreamTag("debugTag", debugTag_tree);
    
    const boost::property_tree::ptree& physicsTag_tree = step.get_child("physicsTag", SELF_TYPE);
    if (physicsTag_tree == SELF_TYPE) {
      std::ostringstream oss; 
      streamPtree(oss, step, "");
      throw(pudummy::AttributeNotFound(ERS_HERE, "physicsTag", "DummyStreamTag", " for step ", uid.c_str(), oss.str().c_str()));
    }
    pudummy::DummyStreamTag * physicsTag = pudummy::DummyStreamTag::addStreamTag("physicsTag", physicsTag_tree);
    
    const boost::property_tree::ptree& calibrationTag_tree = step.get_child("calibrationTag", SELF_TYPE);
    pudummy::DummyCalStreamTag * calibrationTag = 0;
    if (calibrationTag_tree != SELF_TYPE) {
      calibrationTag = pudummy::DummyCalStreamTag::addCalStreamTag(calibrationTag_tree);
    }
    std::vector<pudummy::DataAccess*> roi;
    bool lvl2_step = (step_it->first.compare("DummyROBStep") == 0) ? true:false;
    if (lvl2_step) {
      const boost::property_tree::ptree& roi_tree = step.get_child("roi", SELF_TYPE);
      if (roi_tree == SELF_TYPE) {
        std::ostringstream oss; 
        streamPtree(oss, step, "");
        throw(pudummy::AttributeNotFound(ERS_HERE, "roi", "DummyROBStep", " for step ", uid.c_str(), oss.str().c_str()));
      } else {
        addRoi(uid.c_str(), roi_tree, roi);
      }
      return new pudummy::DummyROBStep(uid, btDist, debugTag, physicsTag, calibrationTag, roi);
    }
    return new pudummy::DummyStep(uid, btDist, debugTag, physicsTag, calibrationTag);
  }
  catch (ers::Issue& ex) { 
    throw(pudummy::ConfigurationIssue(ERS_HERE, "ers::Issue when creating a Dummy[ROB]Step=", uid.c_str(), ex));
  }
  catch (std::exception& ex) { 
    throw(pudummy::ConfigurationIssue(ERS_HERE, "std::exception when creating a Dummy[ROB]Step=", uid.c_str(), ex));
  }
}

/************************************************/
pudummy::Config::Config(const boost::property_tree::ptree & args)
: m_uid(""),
  m_config_tree(args),
  m_result_size(0),
  m_nbrHltTrigInfo(0),
  m_nbrPscErrors(0),
  m_status(true),
  m_burner(),
  m_ignore_bad_rob_access(false),
  m_notEnoughROBs(0),
  m_emptySubdetectorAccess(0)
{
  std::ostringstream oss;
  streamPtree(oss, args, "");
  ERS_DEBUG(0, "Configuring with ptree: '" << oss.str() << "'.");
  // Set m_ignore_bad_rob_access if defined
  char * tmp = getenv("TDAQ_PUDUMMY_IGNORE_BAD_ROB_ACCESS");
  if (tmp) {
    errno = 0;
    size_t val = strtoul(tmp, 0, 0);
    if (errno == 0) {
      m_ignore_bad_rob_access = val ? true:false;
    } else {
      ers::error(ConfigurationIssue(ERS_HERE, "TDAQ_PUDUMMY_IGNORE_BAD_ROB_ACCESS\
 variable in OKS has an invalid value. Warnings are NOT ignored", ""));
    }
  }
  ERS_LOG("Warnings for 'bad' access to ROBs are " 
      << (m_ignore_bad_rob_access ? "" : "not ") 
      << "suppressed.");  
  try {
    const std::string steering_id =
      m_config_tree.get<std::string>("Configuration.Partition.TriggerConfiguration.TriggerConfiguration.hlt.PuDummySteering.UID", DEFAULT_VALUE);
    if (steering_id.compare(DEFAULT_VALUE) == 0) {
      throw(ConfigurationIssue(ERS_HERE, "TriggerConfiguration PuDummySteering not found in configuration", ""));
    }
    // Fill the PuDummySteering attributes.
    const boost::property_tree::ptree pust = 
        m_config_tree.get_child("Configuration.Partition.TriggerConfiguration.TriggerConfiguration.hlt.PuDummySteering");
    std::vector<std::string> params; 
    params.push_back("UID");
    params.push_back("resultSizeDistribution");
    params.push_back("nbrHltTriggerInfo");
    params.push_back("nbrPscErrors");
    params.push_back("seed");
    std::map<std::string, std::string> result;
    const std::pair<const std::string, const boost::property_tree::ptree> pair(steering_id, pust);
    if (!get_ptree_params(pair, params, result)) {
        m_status = false; 
        return;
    }
    m_uid = result["UID"];
    std::string result_size_distribution = result["resultSizeDistribution"];
    m_nbrHltTrigInfo = strtoul(result["nbrHltTriggerInfo"].c_str(), 0, 0);
    m_nbrPscErrors = strtoul(result["nbrPscErrors"].c_str(), 0, 0);
    s_seed = strtol(result["seed"].c_str(), 0, 0);
    m_result_size = new DC::Distribution(result_size_distribution, s_seed);
    ERS_DEBUG(0, "Found PuDummySteering with UID=" << m_uid << ", result_size_distribution='" << result_size_distribution << "', m_nbrHltTrigInfo=" << m_nbrHltTrigInfo << ", m_nbrPscErrors=" << m_nbrPscErrors << ", s_seed=" << s_seed);
    
    // Find all algorithm steps.
    const boost::property_tree::ptree steps = 
        pust.get_child("steps");
    boost::property_tree::ptree::const_iterator it;
    for (it=steps.begin(); it != steps.end(); it++) {
      m_algorithms.push_back(addStep(it));
    }
  } 
  catch (ers::Issue& ex) { 
    ers::fatal(ConfigurationIssue(ERS_HERE, "ers::Issue when creating PuDummySteering=", m_uid.c_str(), ex));
    throw(ConfigurationIssue(ERS_HERE, "ers::Issue when creating PuDummySteering=", m_uid.c_str(), ex));
  }
  catch (std::exception& ex) { 
    ers::fatal(ConfigurationIssue(ERS_HERE, "std::exception when creating PuDummySteering=", m_uid.c_str(), ex));
    throw(ConfigurationIssue(ERS_HERE, "std::exception when creating PuDummySteering=", m_uid.c_str(), ex));
  }
  
  // Configure available ROBs
  size_t ros_count = 0;
  try {
    const boost::property_tree::ptree & ros2robs = 
      args.get_child("Configuration.ROS2ROBS");
    /* Iterate over all childern to find ROS2ROBS subtrees.
     */
    boost::property_tree::ptree::const_iterator ros_it;
    for (ros_it=ros2robs.begin(); ros_it != ros2robs.end(); ros_it++) {
      ERS_DEBUG(0, "Found ROS '" << (*ros_it).first << "'."); 
      ros_count++;
      const boost::property_tree::ptree & robs = (*ros_it).second;
      std::vector<std::string> rob_names;
      BOOST_FOREACH(const boost::property_tree::ptree::value_type &v, robs) {
        rob_names.push_back(v.second.data());
      }
      std::vector<std::string>::const_iterator rob_it = rob_names.begin();
      for (; rob_it != rob_names.end(); rob_it++) {
        errno=0;
        uint32_t rob_id = strtoul((*rob_it).c_str(), 0, 0);
        if (errno) {
          std::ostringstream oss2;
          oss2 << "Invalid ROB ID '" << *rob_it << "' for ";
          ers::warning(pudummy::ConfigurationIssue(ERS_HERE, oss2.str().c_str(), (*ros_it).first.c_str()));
          continue;
        }
        // Check if rob_id already exists 
        std::vector<uint32_t>::const_iterator it3 = find(m_all_robs.begin(), m_all_robs.end(), rob_id);
        if (it3 != m_all_robs.end()) {
          ers::warning(pudummy::DuplicateROBID(ERS_HERE, rob_id));
        } else {
          ERS_DEBUG(0, "Added ROB " << HEX(rob_id) << " for " << (*ros_it).first);
          m_all_robs.push_back(rob_id);
        }
      }
    }
  }
  catch (ers::Issue& ex) {
    throw(ConfigurationIssue(ERS_HERE, "ers::Issue when configuring ROSs", "", ex));
  }
  catch (std::exception& ex) { 
    throw(ConfigurationIssue(ERS_HERE, "std::exception when configuring ROSs", "", ex));
  }
  if (m_all_robs.size() == 0) {
    ers::error(ConfigurationIssue(ERS_HERE, "No ROBS found in configuration", "")); 
    throw(ConfigurationIssue(ERS_HERE, "No ROBS found in configuration", ""));
  }
  std::sort(m_all_robs.begin(), m_all_robs.end());
  for (std::vector<uint32_t>::const_iterator it = m_all_robs.begin(); it != m_all_robs.end(); it++) { 
    unsigned int det_id = *it >> 16 & 0xff;
    m_subdet_robs[det_id].push_back(*it);
    m_subdet_robs[det_id>>4].push_back(*it);
  }
  ERS_DEBUG(0, "Configured PuDummySteering with " << ros_count << " ROSs containing "
      << m_all_robs.size() << " unique ROBs.");
  log_config();
}

/************************************************/
pudummy::Config::~Config()
{ ERS_DEBUG(0, "Delete configuration for PuDummySteering '" << m_uid << "'.");
  delete m_result_size; m_result_size = 0;
  for (std::vector<DummyStep*>::const_iterator it = m_algorithms.begin(); 
      it != m_algorithms.end(); it++) {
    DummyROBStep * lvl2_step = dynamic_cast<DummyROBStep*>(*it);
    if(lvl2_step) {
      delete lvl2_step;
    } else {
      delete (*it);
    }
  }
  m_algorithms.clear();
  
  m_all_robs.clear();
  std::map<unsigned int, std::vector<uint32_t> >::iterator it3 = m_subdet_robs.begin();
  for (; it3 != m_subdet_robs.end(); it3++) {
    it3->second.clear();
  }
  m_subdet_robs.clear();
}

/************************************************
 * Get ROB IDs for one DataAccess object, i.e., part of a RoI or calibration tag.
 */
void pudummy::Config::get_ROB_IDs(uint32_t lvl1_id, 
                const pudummy::DataAccess & dataAccess, 
                std::vector<uint32_t> & ids,
                const std::string & alg_name) {
  unsigned int detector_id = dataAccess.get_subDetector();
  uint32_t nrobs = dataAccess.get_nROBs();
  ERS_DEBUG(2, "ROB access for L1ID=" << lvl1_id << " ROB DummyStep " << alg_name 
      << ": Data access requests for " << nrobs 
      << " ROBs to SubDetector[Group] " << HEX(detector_id) << ".");
  if (nrobs) {
    /* Get nrobs. select_robs() reserves
     * space but does not clear the vector ids.
     */
    select_robs(lvl1_id, nrobs, detector_id, ids);
  } else {
    const std::vector<uint32_t> * robs = 0;
    if (detector_id) {
      // Read all ROBs in the specified Subdetector[Group]
      ERS_DEBUG(1, "L1ID=" << lvl1_id << " DummyROBStep " << alg_name
        << ": ROB request for Subdetector[Group]=" << HEX(detector_id)
        << ".");
      std::map<unsigned int, std::vector<uint32_t> >::const_iterator it =
          m_subdet_robs.find(detector_id);
      if (it == m_subdet_robs.end()) {
        m_emptySubdetectorAccess++;
        if (!m_ignore_bad_rob_access) ers::warning(pudummy::EmptySubdetector(ERS_HERE, lvl1_id, detector_id));
        return;
      }
      robs = &((*it).second);
      // Copy all IDs from robs to ids.
      for (std::vector<uint32_t>::const_iterator it = robs->begin(); it != robs->end(); it++) ids.push_back(*it);
    } else {
        // nrobs and SubDetector are all zero => empty request.
        ERS_DEBUG(3, "L1ID=" << lvl1_id << " DummyROBStep " << alg_name << " made an explicit empty data access.");
        return;
    }
  }
}

/************************************************/
uint32_t pudummy::Config::result_size() const
{
  return m_result_size->next_value();
}

/************************************************/
void pudummy::Config::select_robs(uint32_t lvl1_id, uint32_t asked_robs, unsigned int sd,
		  std::vector<uint32_t>& ids)
{
  size_t initial_robs = ids.size();
  const std::vector<uint32_t>* valid_robs(0);
  if (sd == 0 ) { //eformat::FULL_SD_EVENT
    valid_robs = &m_all_robs;
  } else {
    // else, this is a per detector call
    std::map<unsigned int, std::vector<uint32_t> >::const_iterator
      it = m_subdet_robs.find(sd);
    if (it == m_subdet_robs.end()) {
      m_emptySubdetectorAccess++;
      if (!m_ignore_bad_rob_access) ers::warning(pudummy::EmptySubdetector(ERS_HERE, lvl1_id, sd));
      return; 
    }
    valid_robs = &(it->second);
  }
  if (asked_robs >= valid_robs->size()) {
    ids.insert(ids.end(), valid_robs->begin(), valid_robs->end());
    size_t tmp = ids.size();
    std::sort(ids.begin(), ids.end());
    std::vector<uint32_t>::iterator it_dup = std::unique(ids.begin(), ids.end());
    ids.erase(it_dup, ids.end());
    if (tmp != ids.size() || valid_robs->size() < asked_robs) {
      m_notEnoughROBs += (ids.size()-initial_robs);
      if (!m_ignore_bad_rob_access) ers::warning(pudummy::NotEnoughROBs(ERS_HERE, asked_robs, 
          ids.size()-initial_robs)); 
    }
    return;
  }
  // Check if any of valid_robs are already taken (in ids vector).
  std::vector<uint32_t> valid_robs_net;
  valid_robs_net.reserve(valid_robs->size());
  valid_robs_net.insert(valid_robs_net.end(), valid_robs->begin(), valid_robs->end());
  for (std::vector<uint32_t>::const_iterator it2 = ids.begin(); it2 != ids.end(); it2++) { 
    std::vector<uint32_t>::iterator it3 = find(valid_robs_net.begin(), valid_robs_net.end(), *it2);
    if (it3 != valid_robs_net.end()) valid_robs_net.erase(it3);
  }
  if (valid_robs_net.size() <= asked_robs) {
    ids.insert(ids.end(), valid_robs_net.begin(), valid_robs_net.end());
    if (valid_robs_net.size() < asked_robs) {
      m_notEnoughROBs += (asked_robs-valid_robs_net.size());
      if (!m_ignore_bad_rob_access) ers::warning(pudummy::NotEnoughROBs(ERS_HERE, asked_robs, 
        valid_robs_net.size())); 
    }
    return;
  }
  // Reserve space for additional ROB IDs.
  ids.reserve(ids.size()+asked_robs);
  // Select each ROBs randomly. 
  size_t rob_cnt = 0;
  size_t tries = 0;
  const size_t infinite = asked_robs *100; // Safegueard againts infinite loop.
  while ((rob_cnt < asked_robs) && (tries++ < infinite)) {
    size_t ix = std::rand() % valid_robs_net.size(); 
    std::vector<uint32_t>::const_iterator rob_it =
        std::find(ids.begin(), ids.end(), (*valid_robs)[ix]);
    if (rob_it == ids.end()) {
      ids.push_back((*valid_robs)[ix]);
      ERS_DEBUG(3, "ROB " << HEX((*valid_robs)[ix]));
      rob_cnt++;
    }
  }
  if (tries >= infinite) {
    ers::warning(pudummy::FailedToRetrieveROBs(ERS_HERE, rob_cnt, asked_robs,
         tries, valid_robs_net.size(), sd));
  }
  return;
}

/************************************************/
void pudummy::Config::log_config() const
{
  std::ostringstream oss;
  for (size_t ix = 0; ix<m_algorithms.size(); ix++) {
    oss << "\n" << m_algorithms[ix]->get_uid() << ": BurnTime-string='" << m_algorithms[ix]->get_burn_time_string() << "', type='" << m_algorithms[ix]->get_burn_time_type() << "'";
  }
  size_t subdetector_count = 0;
  for (std::map<unsigned int, std::vector<uint32_t> >::const_iterator it = m_subdet_robs.begin(); 
      it != m_subdet_robs.end(); it++) {
    if ((*it).first > 0x0f) subdetector_count++;
  }
  ERS_LOG("\n\tConfiguration setting for PuDummySteering=" << m_uid
    << "\n\tConfiguration status=" << bool2str(m_status)
    << "\n\tHLT result size distribution='" << m_result_size->get_DistributionString()
    << "'\n\tTrigger Info words=" << m_nbrHltTrigInfo
    << "\n\tPsc Error words=" << m_nbrPscErrors
    << "\n\trandom seed parameter=" << s_seed
    << "\n\tNumber of algorithm steps=" << m_algorithms.size() << ":" << oss.str() 
    << "\n\t" << m_all_robs.size() << " configured ROBs in " << subdetector_count << " sub detectors.\n");
}

/************************************************/
void pudummy::Config::reseed_gRandom()
{ 
  // Re-seed gRandom ?
  char * tmp = getenv("TDAQ_PUDUMMY_RAND_RESEED");
  char * no_conv;
  if (tmp == 0) return;
  errno = 0;
  int value = strtol(tmp, &no_conv, 0);
  if (tmp != no_conv) {
    s_seed = value;
  } else {
    ers::warning(ConfigurationIssue(ERS_HERE, "TDAQ_PUDUMMY_RAND_RESEED\
 variable in OKS has an invalid value=", tmp));
    return;
  }  
   
  if (s_seed == 0) {
    auto now = std::chrono::high_resolution_clock::now(); 
    s_reseed_value = now.time_since_epoch().count();
  } else if (s_seed < 0) {
    s_reseed_value = getpid();
  } else {
    std::random_device rd;
    s_reseed_value = rd();
  }
  gRandom->SetSeed(s_reseed_value); 
  ERS_LOG("Root random generator 'gRandom' was re-seeded with value=" 
    << s_reseed_value << " from seed parameter=" << s_seed);
}
