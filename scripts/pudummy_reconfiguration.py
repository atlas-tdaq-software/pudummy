#!/usr/bin/env tdaq_python
"""
Set pudummy reconfiguration object HLTConf on multiple IS servers (e.g. all TDAQ_IS_SERVERS)/PW 2013-12-06.
Original script from A.Negri.
"""

from ispy import IPCPartition, ISInfoDynAny, ISInfoDictionary, ISServerIterator

def publish(part_name, is_server_prefix, name, config_file):
  part = IPCPartition(part_name)
  is_dict = ISInfoDictionary(part)
  cfile = open(config_file, 'r')
  value = ''
  list = cfile.readlines()
  for line in list:
    value = value + line
  info = ISInfoDynAny(part, 'String')
  info.value = value
  is_server_it = ISServerIterator(part)
  while is_server_it.next():
    if is_server_it.name().startswith(is_server_prefix):
      is_target = is_server_it.name() + '.' + name
      print "Publishing content of file='%s' to IS target='%s'" % (config_file, is_target)
      if is_dict.contains(is_target):
        is_dict.update(is_target, info)
      else:
        is_dict.insert(is_target, info)

if __name__ == '__main__':
  import argparse
  parser = argparse.ArgumentParser(description='Write new pudummy configuration in IS.')

  pudummy_group = parser.add_argument_group('pudummy Parameters',
    'Write new pudummy configuration in IS object HLTConf for each $TDAQ_IS_SERVER.')
  pudummy_group.add_argument('-v', dest='HLTConf', metavar='STRING',
                         help='temp test of setting HLTConf')
  """
  pudummy_group.add_argument('-ef-a', dest='efAcceptance', type=float, metavar='FLOAT',
                         help='EF Acceptance')
  pudummy_group.add_argument('-ef-ptd', dest='efProcessingTimeDistribution', metavar='STRING',
                         help='EF Processing Time Distribution (ms)')
  pudummy_group.add_argument('-dc-credits', dest='dcCredits', type=int, metavar='INTEGER',
                         help='Data collection traffic shaping credits')
  """

  parser.add_argument('-p', dest='part_name', required=True, 
                      metavar='STRING', help='partition name')
  parser.add_argument('-s', dest='is_server_prefix', required=True,
                      metavar='STRING', help='TDAQ_IS_SERVER prefix')
  parser.add_argument('-f', dest='config_file', metavar='STRING', required=True,
                         help='New pudummy configuration file')
  args = parser.parse_args()

  params = ['{0}={1}'.format(k, v) for k, v in vars(args).iteritems() if k != 'part_name' and v is not None]
  publish(args.part_name, args.is_server_prefix, 'HLTConf', args.config_file)
